<?php

namespace Drupal\path_corrector\Plugin\Filter;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;

/**
 * Provides a filter to limit allowed HTML tags.
 *
 * @Filter(
 *   id = "path_corrector",
 *   title = @Translation("Rewrite/correct links and URLs in content."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {
 *     "tags" = {
 *       "a" = "0",
 *       "img" = "0",
 *     },
 *     "string_replacements" = "example.com|example.org"
 *   },
 *   weight = -10
 * )
 */
class FilterPathCorrector extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['tags'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Tags'),
      '#description' => $this->t('Which tags (HTML Elements) should be corrected'),
      '#default_value' => $this->settings['tags'],
      // NOTE: These are intentionally un-t()'d...
      '#options' => [
        'a' => '&lt;a href="">',
        'img' => '&lt;img src="">',
      ],
    ];

    $form['string_replacements'] = [
      '#type' => 'textarea',
      '#title' => $this->t('String Replacements'),
      '#default_value' => $this->settings['string_replacements'],
      '#description' => $this->t('Enter a replacement "pairs", one replacement per line. <br />') .
      $this->t('Each replacements should be in pipe-separated form: <code>example.com|example.org</code><br />'),
    ];

    return $form;
  }

  /**
   * Convert the string_replacements value into an array.
   *
   * The string is a newline separated list of pipe-separated pairs of URL parts
   * to replace.
   *
   * For example:
   *   example.com|example.org
   *
   * @return array
   *   Array of From => To pairs of replacements.
   */
  private function getStringReplacementPairs() {
    $str_replace_pairs = [];
    foreach (explode("\n", $this->settings['string_replacements']) as $replacement) {
      if (strpos($replacement, '|') !== FALSE) {
        list($from, $to) = explode('|', $replacement);
        $str_replace_pairs[trim($from)] = UrlHelper::encodePath(trim($to));
      }
    }
    return $str_replace_pairs;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    // Bail early if there are no replacements.
    if (empty($this->settings['string_replacements'])) {
      return $text;
    }
    $patterns = [];

    if (isset($this->settings['tags']['a'])) {
      $patterns[] = '/<a.+?href=["\'][^\'"]+["\']/';
    }
    if (isset($this->settings['tags']['img'])) {
      $patterns[] = '/<img.+?src=["\'][^\'"]+["\']/';
    }

    $string_replacement_pairs = $this->getStringReplacementPairs();
    $callback = function ($matches) use ($string_replacement_pairs) {
      return str_replace(array_keys($string_replacement_pairs), $string_replacement_pairs, $matches[0]);
    };

    $text = preg_replace_callback($patterns, $callback, $text);
    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $string_replacement_pairs = $this->getStringReplacementPairs();
    array_walk($string_replacement_pairs, function (&$value, $key) {
      $value = $key . ' » ' . $value;
    });
    return $this->t('The following paths will be corrected: @source_paths', ['@source_paths' => implode(', ', $string_replacement_pairs)]);
  }

}
