<?php

namespace Drupal\path_corrector\Tests;

use Drupal\filter\FilterPluginCollection;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests Filter module filters individually.
 *
 * @group filter
 */
class PathCorrectorUnitTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['system', 'filter', 'path_corrector'];

  /**
   * Array of filters, populated during setUp.
   *
   * @var \Drupal\filter\Plugin\FilterInterface[]
   */
  protected $filters;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['system']);

    $manager = $this->container->get('plugin.manager.filter');
    $bag = new FilterPluginCollection($manager, []);
    $this->filters = $bag->getAll();
  }

  /**
   * Tests the path corrector filter.
   */
  public function testPathCorrectorFilter() {
    $filter = $this->filters['path_corrector'];

    $test = function ($input) use ($filter) {
      return $filter->process($input, 'und');
    };

    // Image SRC - no change.
    $input = '<img src="http://example.co.uk/test.jpg" />';
    $expected = $input;
    $this->assertSame($expected, $test($input)->getProcessedText());

    // Image SRC - change.
    $input    = '<img src="http://example.com/test.jpg" />';
    $expected = '<img src="http://example.org/test.jpg" />';
    $this->assertSame($expected, $test($input)->getProcessedText());

    // Hyperlink HREF - no change.
    $input = '<a href="http://example.co.uk/test" />';
    $expected = $input;
    $this->assertSame($expected, $test($input)->getProcessedText());

    // Hyperlink HREF - change.
    $input    = '<a href="http://example.com/test" />';
    $expected = '<a href="http://example.org/test" />';
    $this->assertSame($expected, $test($input)->getProcessedText());
  }

}
